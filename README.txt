
Skripte zur Interaktion mit der SRW-Implementierung der ZESAR-Spezifikation.

Die Skripte dienen dazu, den von SRW implementierten, generischen ZESAR-Server
mit Daten zu befüllen bzw. Abrechnungsdaten von dort abzurufen.


zesar-client
=============
Dieses Skript kann Rezepte zum Server hochladen. Die Rezeptdaten müssen bereits
im von ZESAR geforderten XML-Format vorliegen.



zesar-billing
=============

Benutzung:
$ zesar-billing <START> <ENDE>
z.B.: $ zesar-billing 01.04.2015 30.04.2015

Das Skript erstellt eine Auswertung der von ZESAR abgerufenen Rezepte und sowie
die dafür akzeptierten Abrufgebühren.

