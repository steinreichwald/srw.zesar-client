#!/usr/bin/env python

import re

from setuptools import setup, find_packages

def requires_from_file(filename):
    requirements = []
    with open(filename, 'r') as requirements_fp:
        for line in requirements_fp.readlines():
            match = re.search('^\s*([a-zA-Z][^#]+?)(\s*#.+)?\n$', line)
            if match:
                requirements.append(match.group(1))
    return requirements


setup(
    name='ZesarClient',
    version='0.1',

    author='Felix Schwarz',
    author_email='info@schwarz.eu',
    license='proprietary',

    zip_safe=False,
    packages=find_packages(),
    namespace_packages = ['srw'],
    include_package_data=True,

    install_requires=requires_from_file('requirements.txt'),
    entry_points="""
        [console_scripts]
        zesar-client = srw.zesar_client.uploader:main
        zesar-billing = srw.zesar_client.billing:main
    """,
)

