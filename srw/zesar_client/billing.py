# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

try:
    from configparser import SafeConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser
from datetime import datetime as DateTime, time as Time
from decimal import Decimal
import os
import sys


from babel.dates import format_datetime, parse_date
from babel.numbers import parse_pattern
from soapfish.lib.attribute_dict import AttrDict

# need to import AdminSchema to initialize SCHEMA attribute on request types
# correctly.
from srw.zesar_client.admin_schema import AdminSchema
from srw.zesar_client.queryRecordedRequests import endpoint_data
from srw.zesar_client.queryRecordedRequests.request_types import *
from srw.zesar_client.soap_client import ZESARClient


reduction_explanations = {
    10: 'TAN-Satz bei ZESAR nicht vorhanden',
    20: 'Keine Daten zu TAN-Satz geliefert',
    30: 'TAN-Satz bereits abgerechnet',
    40: 'TAN-Satz mehrfach angeliefert',
}

def parse_config():
    parser = SafeConfigParser()
    parser.read(['zesar.ini', os.path.expanduser('~/.zesar.ini')])
    return AttrDict(parser.items('zesar'))


def cents_as_euro_string(cents):
    euros = Decimal(cents) / 100
    pattern_de = parse_pattern("#,##0.00")
    return pattern_de.apply(euros, locale='de_DE')


def cents_as_delta_euro_string(cents):
    value_str = cents_as_euro_string(cents) + ' Euro'
    if cents < 0:
        return value_str
    return '+' + value_str


def report_for_pharmacy(pharmacy_id, requests, verbose=True):
    sum_pharmacy = 0
    sum_datacenter = 0
    billed_requests = 0
    unbilled_retrievals = []
    reduced_fees = []
    for request in requests:
        if request.zeitpunktAkzeptanz is None:
            unbilled_retrievals.append(request)
            continue
        sum_pharmacy += request.akzeptiertApotheke
        sum_datacenter += request.akzeptiertARZ
        billed_requests += 1
        if (request.akzeptiertApotheke != request.forderungApotheke) or (request.akzeptiertARZ != request.forderungARZ):
            reduced_fees.append(request)

    print('Apotheke %s' % pharmacy_id)
    # This assumes a "reduced" fee is actually an unpaid request
    # (e.g. duplicate request by ZESAR). Currently true but might need checking
    # when the ZESAR protocol changes.
    paid_requests = billed_requests - len(reduced_fees)
    print('    %d Abrufe abgerechnet, %d (noch?) nicht akzeptiert' % (paid_requests, len(unbilled_retrievals)))
    print('    Rechnungssumme Apotheke %s Euro' % cents_as_euro_string(sum_pharmacy))
    print('    Rechnungssumme ARZ %s Euro' % cents_as_euro_string(sum_datacenter))
    if len(reduced_fees) > 0 or verbose:
        print('\n    %d Abrufe mit geminderten Gebuehren' % len(reduced_fees))
        for reduced in reduced_fees:
            explanations = []
            for code in reduced.minderungsgruende:
                explanation = reduction_explanations.get(code, 'Unbekannter Minderungsgrund (%r)' % code)
                explanations.append(explanation)
            string_minderung = ', '.join(explanations)
            string_dt = format_datetime(reduced.abrufzeit, locale='de_DE')
            indent = '      '
            print(indent + 'TAN %s (Abruf %s durch %s): %s' % (reduced.tan, string_dt, reduced.username, string_minderung))

            deltas = []
            apo_delta = (reduced.akzeptiertApotheke - reduced.forderungApotheke)
            if (apo_delta != 0) and (reduced.akzeptiertApotheke != 0):
                deltas.append('APO: %s' % cents_as_delta_euro_string(apo_delta))
            arz_delta = (reduced.akzeptiertARZ - reduced.forderungARZ)
            if (arz_delta != 0) and (reduced.akzeptiertARZ != 0):
                deltas.append('ARZ: %s' % cents_as_delta_euro_string(arz_delta))
            if deltas:
                delta_indent = indent + '    '
                print(delta_indent + ', '.join(deltas))

    if len(unbilled_retrievals) > 0 or verbose:
        print('\n    %d noch nicht akzeptierte Abrufe' % len(unbilled_retrievals))
        for unbilled in unbilled_retrievals:
            string_dt = format_datetime(unbilled.abrufzeit, locale='de_DE')
            print('      TAN %s (Abruf %s durch %s)' % (unbilled.tan, string_dt, unbilled.username))

    return sum_pharmacy + sum_datacenter

def main(argv=sys.argv):
    if len(argv) < 3:
        print('usage: %r START END' % argv[0])
        sys.exit(1)

    settings = parse_config()
    client = ZESARClient(
        ws_url=settings.url,
        soap_action=endpoint_data.info.soap_action,
        request_tag=endpoint_data.info.input,
        response_object=endpoint_data.info.response_object,
        credentials=(settings.user, settings.password)
    )

    start_date = parse_date(argv[1], locale='de_DE')
    start = DateTime.combine(start_date, Time(0, 0, 0))
    end_date = parse_date(argv[2], locale='de_DE')
    end = DateTime.combine(end_date, Time(23, 59, 59))

    # stub for manual tests
    # xml = open('...').read()
    # billing_response = client._parse_soap(xml)
    query_request = RecordedRequestsQuery.create(start=start, end=end)
    billing_response = client.query_recorded_requests(query_request)

    logged_requests = {}
    for billed_request in billing_response.recorded_requests:
        # LATER: do not hard-code "zesar" username
        if billed_request.username != 'zesar':
            # ignore test requests by other users
            continue
        pharmacy_id = billed_request.apoIK
        if pharmacy_id not in logged_requests:
            logged_requests[pharmacy_id] = []
        logged_requests[pharmacy_id].append(billed_request)

    sum_zesar = 0
    for pharmacy_id, requests in logged_requests.items():
        sum_zesar += report_for_pharmacy(pharmacy_id, requests)
        print()
    print('================================================\n')
    print('    Gesamt-Rechnungssumme an ZESAR %s Euro\n' % cents_as_euro_string(sum_zesar))


if __name__ == '__main__':
    main()
