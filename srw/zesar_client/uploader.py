# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function, unicode_literals

try:
    from configparser import SafeConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser
import logging
import os
import sys

from soapfish.lib.attribute_dict import AttrDict

from .admin_schema import AdminSchema
from .importPrescriptionData import endpoint_data
from .importPrescriptionData.request_types import *
from .lib.srw_simplelog import setup_logging
from .soap_client import ZESARClient

def parse_config():
    parser = SafeConfigParser()
    parser.read(['zesar.ini', os.path.expanduser('~/.zesar.ini')])
    return AttrDict(parser.items('zesar'))


def data_files(paths):
    file_names = []
    for path in paths:
        if os.path.isfile(path):
            file_names.append(path)
            continue
        for name in os.listdir(path):
            file_path = os.path.join(path, name)
            if os.path.isfile(file_path):
                file_names.append(file_path)
            else:
                file_names.extend(data_files([file_path]))
    return file_names

def main(argv=sys.argv):
    if len(argv) < 2:
        print('usage: %r DATADIR' % argv[0])
        sys.exit(1)
    log = setup_logging()
    # prevent log spam ("Starting new HTTP connection …")
    logging.getLogger('requests').setLevel(logging.WARNING)

    settings = parse_config()
    client = ZESARClient(
        ws_url=settings.url,
        soap_action=endpoint_data.info.soap_action,
        request_tag=endpoint_data.info.input,
        response_object=endpoint_data.info.response_object,
        credentials=(settings.user, settings.password)
    )

    for data_file in data_files(argv[1:]):
        with open(data_file, 'r') as fp:
            xml = fp.read()
        import_request = ImportRequest.parsexml(xml, schema=AdminSchema)

        import_response = client.submit_prescriptions(import_request)
        for p in import_response.prescriptions:
            log_str = f'TAN {p.tan} ({p.abgabedatum.as_datetime_date()}): ID {p.id}'
            log.info(log_str)
            # prevent output buffering (show progress)
            sys.stdout.flush()

if __name__ == '__main__':
    main()
