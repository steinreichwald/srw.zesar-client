# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from soapfish import xsd

from .definitions import srw_namespace
from .importPrescriptionData.request_types import *
from .importPrescriptionData.response_types import *
from .queryRecordedRequests.request_types import *
from .queryRecordedRequests.response_types import *


__all__ = ['AdminSchema']

AdminSchema = xsd.Schema(
    targetNamespace=srw_namespace,
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    complexTypes=(
        ImportedPrescription,
        ImportRequest,
        ImportResponse,
        PrescribedItem,
        PrescriptionData,

        RecordedRequestsQuery,
        RecordedRequestLog,
        RecordedRequestInfo
    ),
    elements={
        'importRequest': xsd.Element(ImportRequest),
        'importResponse': xsd.Element(ImportResponse),
        'recordedRequestsQuery': xsd.Element(RecordedRequestsQuery),
        'recordedRequestLog': xsd.Element(RecordedRequestLog),
    },
)

