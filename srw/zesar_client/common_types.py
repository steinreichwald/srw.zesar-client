# -*- coding: utf-8 -*-

from soapfish import xsd


__all__ = ['create_', 'IKNrTyp', 'TANTyp']

def create_(cls, **kwargs):
    instance = cls()
    field_names = [field._name for field in instance._meta.all]
    for key, value in kwargs.items():
        if key not in field_names:
            raise TypeError('create() got an unexpected keyword argument %r' % key)
        setattr(instance, key, value)
    return instance


class IKNrTyp(xsd.String):
    pattern = r'[0-9]{9}'


class TANTyp(xsd.String):
    pattern = r'[0-9]{9}'

    @classmethod
    def create(cls, value):
        type_ = TANTyp()
        type_.value = value
        return type_
