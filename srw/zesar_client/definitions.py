# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals


__all__ = ['srw_namespace', 'xsd_baseurl']

srw_namespace = 'https://www.steinreichwald.net/zesar/1.0'

ws_location = 'https://www.steinreichwald.net/zesar/'
ws_admin_location = ws_location + 'admin/'
xsd_baseurl = ws_location + 'static/xsd/'
