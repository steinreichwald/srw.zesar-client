# -*- coding: utf-8 -*-

import logging
from logging.config import fileConfig as init_logging_from_log_conf
import os
import sys

__all__ = ['setup_logging']

ENV_NAME = 'SRWLOG'

def guess_logpath(path_logfile=None):
    if path_logfile is not None:
        path_logfile = os.path.abspath(path_logfile)
    elif ENV_NAME in os.environ:
        path_logfile = os.environ[ENV_NAME]
    else:
        exe_filename = os.path.basename(sys.argv[0])
        basename = os.path.splitext(exe_filename)[0] if ('.' in exe_filename) else exe_filename
        path_logfile = os.path.abspath(basename + '.log')
    return path_logfile

def setup_logging(path_logfile=None, logger_name=None):
    path_logfile = guess_logpath(path_logfile)
    dir_logfile = os.path.dirname(path_logfile)
    if not os.path.exists(dir_logfile):
        msg = 'Logdatei "%s" kann nicht erzeugt werden: Verzeichnis existiert nicht.\n' % dir_logfile
        sys.stderr.write(msg)
        sys.exit(5)

    is_logging_conf = path_logfile.lower().endswith('.conf')
    if is_logging_conf:
        init_logging_from_log_conf(path_logfile, disable_existing_loggers=True)
    log = logging.getLogger(logger_name)
    if not is_logging_conf:
        file_ = logging.FileHandler(path_logfile)
        file_.setLevel(logging.DEBUG)
        file_.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s] %(message)s'))
        log.addHandler(file_)
        stderr = logging.StreamHandler(stream=sys.stderr)
        stderr.setLevel(logging.INFO)
        log.addHandler(stderr)
        # ensure that this logger actually forwards everything (output depends
        # on handler level)
        log.setLevel(logging.DEBUG)
    return log
