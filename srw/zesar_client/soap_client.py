
import requests
from soapfish.core import SOAPError
from soapfish.soap import SOAPVersion

from .basic_auth import BasicAuth, BasicAuthHeader


class SOAPClient(object):
    def __init__(self, ws_url, soap_action, request_tag, response_object):
        self.ws_url = ws_url
        self.soap_action = soap_action
        self.request_tag = request_tag
        self.response_object = response_object

    def submit_request(self, request_data, soap_header=None):
        headers = {
            'ACCEPT': 'application/soap+xml,text/xml',
            'SOAPAction': '"%s"' % self.soap_action,
        }
        SOAP = SOAPVersion.SOAP11
        envelope = SOAP.Envelope.response(self.request_tag, request_data, header=soap_header)
        return requests.post(self.ws_url, data=envelope, headers=headers)

    def _parse_soap(self, xml):
        SOAP = SOAPVersion.SOAP11
        envelope = SOAP.Envelope.parsexml(xml)
        if envelope.Body.Fault is not None:
            code, message, actor = SOAP.parse_fault_message(envelope.Body.Fault)
            error = SOAPError(code=code, message=message, actor=actor)
            raise error

        body_content = envelope.Body.content()
        return self.response_object.parse_xmlelement(body_content)

    def parse_response(self, response):
        if response.status_code != 200:
            # well, not really nice to use print for status messages but it
            # is actually the fastest way for now.
            print(response.text)
            raise ValueError('web service returned status code %d' % response.status_code)
        return self._parse_soap(response.text)


class ZESARClient(SOAPClient):
    def __init__(self, credentials=None, *args, **kwargs):
        super(ZESARClient, self).__init__(*args, **kwargs)
        self.credentials = credentials

    def _auth_header(self):
        if self.credentials is None:
            return None
        username, password = self.credentials
        basic_auth = BasicAuth.create(Name=username, Password=password)
        return BasicAuthHeader.create(BasicAuth=basic_auth)

    def query_recorded_requests(self, query_request):
        response = self.submit_request(query_request, soap_header=self._auth_header())
        return self.parse_response(response)

    def submit_prescriptions(self, import_request):
        response = self.submit_request(import_request, soap_header=self._auth_header())
        return self.parse_response(response)

