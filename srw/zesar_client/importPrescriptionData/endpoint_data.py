# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from soapfish.lib.attribute_dict import AttrDict

from ..definitions import ws_admin_location
from .response_types import *

__all__ = ['info']

info = AttrDict(
    operation_name='importPrescriptionData',
    soap_action=ws_admin_location + 'importPrescriptionData',
    input='importRequest',
    response_object=ImportResponse,
)

