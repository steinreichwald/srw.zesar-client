# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_


__all__ = ['ImportedPrescription', 'ImportResponse']

class ImportedPrescription(xsd.ComplexType):
    id = xsd.Element(xsd.Integer)
    apoIK = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    tan = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    abgabedatum = xsd.Element(xsd.Date)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class ImportResponse(xsd.ComplexType):
    prescriptions = xsd.ListElement(
        ImportedPrescription,
        'parenteraliaSatz',
        minOccurs=1,
        maxOccurs=xsd.UNBOUNDED
    )

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

