# -*- coding: utf-8 -*-

from soapfish import xsd

from ..common_types import create_


__all__ = ['ImportRequest', 'PrescribedItem', 'PrescriptionData']

class PrescribedItem(xsd.ComplexType):
    pzn = xsd.Element(xsd.String(pattern='[0-9]{7,8}'))
    faktor = xsd.Element(xsd.Integer(totalDigits=10))

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class PrescriptionData(xsd.ComplexType):
    apoIK = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    tan = xsd.Element(xsd.String(pattern='[0-9]{9}'))
    abgabedatum = xsd.Element(xsd.Date)
    sonderkennzeichen = xsd.Element(xsd.String(pattern='[0-9]{7,8}'))
    taxe = xsd.Element(xsd.Integer(totalDigits=14)) # cents
    waehrungskennzeichen = xsd.Element(xsd.String(pattern='EUR'))
    items = xsd.ListElement(PrescribedItem, 'einzelbestandteil', minOccurs=1, maxOccurs=xsd.UNBOUNDED)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


class ImportRequest(xsd.ComplexType):
    prescriptions = xsd.ListElement(
        PrescriptionData,
        'parenteraliaSatz',
        minOccurs=1,
        maxOccurs=xsd.UNBOUNDED
    )

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

