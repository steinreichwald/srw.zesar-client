# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from soapfish import xsd

from .common_types import create_
from .definitions import xsd_baseurl


__all__ = ['BasicAuth', 'BasicAuthSchema', 'BasicAuthHeader']

soap_auth_ns = 'http://soap-authentication.org/basic/2001/10/'

class BasicAuth(xsd.ComplexType):
    Name = xsd.Element(xsd.String, namespace=soap_auth_ns)
    Password = xsd.Element(xsd.String, namespace=soap_auth_ns)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)


BasicAuthSchema = xsd.Schema(
    targetNamespace=soap_auth_ns,
    location=xsd_baseurl + '/BasicAuthSchema.xsd',
    elementFormDefault=xsd.ElementFormDefault.QUALIFIED,
    complexTypes=(
        BasicAuth,
    ),
    elements={
        'BasicAuth': xsd.Element(__name__ + '.BasicAuth'),
    },
)


class BasicAuthHeader(xsd.ComplexType):
    SCHEMA = BasicAuthSchema
    BasicAuth = xsd.Element(BasicAuth, namespace=soap_auth_ns)

    @classmethod
    def create(cls, **kwargs):
        return create_(cls, **kwargs)

